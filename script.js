/* 1. Прототипне наслідування - це механізм успадкування властивостей і методів
 від батькіських об'єктів до дочірніх. За його допомогою у доірніх об'єктах ми
 маємо можливість звертатися і використовувати властивості і методи об'єктів-прототипів 
 використовуваного об'єкта.
2. Метод super() потрібен для виклику конструктора та методів з батьківсього класу.*/

class Employee {
    constructor(value){
        this.name = value.name;
        this.age = value.age;
        this.salary = value.salary;
    }
    get nameGeter (){
        return this.name
    }
    get ageGeter (){
        return this.age
    }
    get salaryGeter (){
        return this.salary
    }
    set nameSetter(newName){
        this.name = newName
    }
    set ageSetter(newAge){
        this.age = newAge
    }
    set salarySetter(newSalary){
        this.salary = newSalary
    }

}

class Programmer extends Employee{
    constructor(value){
        super(value);
        this.lang = value.lang;
    }
    get salaryGeter(){
        return `${parseFloat(this.salary.replace(/\D/g, ''))*3}$`;
    }
}

const juniorProgrammer = new Programmer({
    name: 'John',
    age: 25,
    salary: '500$',
    lang:{
        lang1: "JS",

    }
})

juniorProgrammer.salarySetter = "300$";

console.log(juniorProgrammer)
console.log(juniorProgrammer.salaryGeter)

const middleProgrammer = new Programmer({
    name: 'George',
    age: 32,
    salary: '1200$',
    lang:{
        lang1: "JS",
        lang2: "Python",
        lang3: "C++",
    }
})
console.log(middleProgrammer)
console.log(middleProgrammer.lang)

const seniorProgrammer = new Programmer({
    name: 'Alex',
    age: 35,
    salary: '5000$',
    lang:{
        lang1: "JS",
        lang2: "Python",
        lang3: "Java"
    }
})
console.log(seniorProgrammer)
console.log(seniorProgrammer.salary)
console.log(seniorProgrammer.salaryGeter)


